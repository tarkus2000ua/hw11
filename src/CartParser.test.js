import CartParser from './CartParser';
import {
	wrongData,
	cleanData,
	result,
	errors
} from './mockData';
import path from 'path';


jest.mock('uuid', () => ({
	v4: () => '00000000-0000-0000-0000-000000000000'
}));


let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	describe('validate method', () => {
		it('csv header names should be verified', () => {
			expect(parser.validate(wrongData)).toContainEqual({
				"column": 0,
				"message": "Expected header to be named \"Product name\" but received Product.",
				"row": 0,
				"type": "header"
			})
			expect(parser.validate(wrongData)).toContainEqual({
				"column": 1,
				"message": "Expected header to be named \"Price\" but received Prize.",
				"row": 0,
				"type": "header"
			})
			expect(parser.validate(wrongData)).toContainEqual({
				"column": 2,
				"message": "Expected header to be named \"Quantity\" but received Quant.",
				"row": 0,
				"type": "header"
			})
		})

		it('csv row should have number of cells according to schema', () => {
			expect(parser.validate(wrongData)).toContainEqual({
				"column": -1,
				"message": "Expected row to have 3 cells but received 2.",
				"row": 1,
				"type": "row"
			})
		})

		it('csv cell should be a nonempty string', () => {
			expect(parser.validate(wrongData)).toContainEqual({
				"column": 0,
				"message": "Expected cell to be a nonempty string but received \"\".",
				"row": 3,
				"type": "cell"
			})
		})

		it('csv cell should be a positive number', () => {
			expect(parser.validate(wrongData)).toContainEqual({
				"column": 2,
				"message": "Expected cell to be a positive number but received \"-1\".",
				"row": 2,
				"type": "cell"
			})
		})

		it('should return an empty array for valid input data', () => {
			expect(parser.validate(cleanData)).toEqual([])
		})
	})

	describe('parseLine method', () => {
		const csvLine = 'Consectetur adipiscing,28.72,10';
		it('should be an object with uuid identifier', () => {
			expect(parser.parseLine(csvLine)).toEqual({
				"id": "00000000-0000-0000-0000-000000000000",
				"name": "Consectetur adipiscing",
				"price": 28.72,
				"quantity": 10
			})
		})
	})

	describe('createError method', () => {
		it('should return object', () => {
			expect(parser.createError('header', 0, 0, 'header error text')).toEqual({
				"column": 0,
				"message": "header error text",
				"row": 0,
				"type": "header",
			})
		})
	});

	describe('calcTotal method', () => {
		const items = result.items;
		it('should return total of 348.32', () => {
			expect(parser.calcTotal(items)).toBeCloseTo(348.32)
		})
	});

	describe('parse method', () => {
		it('should throw validation error', () => {
			const readFile = jest.fn(x => x);
			parser.readFile = readFile.bind(parser);
			expect(() => parser.parse(wrongData)).toThrow('Validation failed!');
		})

		it('should console error array of errors', () => {
			const readFile = jest.fn(x => x);
			parser.readFile = readFile.bind(parser);
			const consoleSpy = jest.spyOn(console, 'error');
			expect(() => parser.parse(wrongData)).toThrow('Validation failed!');
			expect(consoleSpy).toBeCalledWith(errors);
		})

		it('should return object', () => {
			const readFile = jest.fn(x => x);
			parser.readFile = readFile.bind(parser);
			expect(parser.parse(cleanData)).toEqual(result);
		})
	});

});

describe('CartParser - integration test', () => {
	it('should return object', () => {
		const csvFile = path.join(__dirname, '../samples', 'cart.csv');
		expect(parser.parse(csvFile)).toEqual(result);
	})
});