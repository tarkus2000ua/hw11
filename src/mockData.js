export const wrongData = `Product,Prize,Quant
Mollis consequat,9.00
Tvoluptatem,10.32,-1
,,
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1`

export const cleanData = `Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1`

export const result = {
	"items": [{
			"id": "00000000-0000-0000-0000-000000000000",
			"name": "Mollis consequat",
			"price": 9,
			"quantity": 2
		},
		{
			"id": "00000000-0000-0000-0000-000000000000",
			"name": "Tvoluptatem",
			"price": 10.32,
			"quantity": 1
		},
		{
			"id": "00000000-0000-0000-0000-000000000000",
			"name": "Scelerisque lacinia",
			"price": 18.9,
			"quantity": 1
		},
		{
			"id": "00000000-0000-0000-0000-000000000000",
			"name": "Consectetur adipiscing",
			"price": 28.72,
			"quantity": 10
		},
		{
			"id": "00000000-0000-0000-0000-000000000000",
			"name": "Condimentum aliquet",
			"price": 13.9,
			"quantity": 1
		}
	],
	"total": 348.32
}

export const errors = [
	{
		type: 'header',
		row: 0,
		column: 0,
		message: 'Expected header to be named "Product name" but received Product.'
	},
	{
		type: 'header',
		row: 0,
		column: 1,
		message: 'Expected header to be named "Price" but received Prize.'
	},
	{
		type: 'header',
		row: 0,
		column: 2,
		message: 'Expected header to be named "Quantity" but received Quant.'
	},
	{
		type: 'row',
		row: 1,
		column: -1,
		message: 'Expected row to have 3 cells but received 2.'
	},
	{
		type: 'cell',
		row: 2,
		column: 2,
		message: 'Expected cell to be a positive number but received "-1".'
	},
	{
		type: 'cell',
		row: 3,
		column: 0,
		message: 'Expected cell to be a nonempty string but received "".'
	}
]